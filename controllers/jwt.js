const expressJwt = require('express-jwt');

//https://www.npmjs.com/package/express-jwt
//https://www.codegrepper.com/code-examples/javascript/frameworks/nodejs/
//https://www.fatalerrors.org/a/0d1w1Ds.html
//https://stackabuse.com/authentication-and-authorization-with-jwts-in-express-js/
function authJwt() {
    const secret = process.env.secret;
    const api = process.env.API_URL;
    return expressJwt({
        secret,
        algorithms: ['HS256'],
		credentialsRequired: true,
        isRevoked: isRevoked
    }).unless({
        path: [
            {url: /\/api\/v1\/users(.*)/ , methods: ['GET', 'OPTIONS'] },
			{url: /\/api\/v1\/products(.*)/ , methods: ['GET', 'OPTIONS'] },
            {url: /\/api\/v1\/categories(.*)/ , methods: ['GET', 'OPTIONS'] },
            {url: /\/api\/v1\/orders(.*)/ , methods: ['GET', 'OPTIONS'] },
            `${api}/users/login`,
            `${api}/users/register`,
        ]
    })
}

async function isRevoked(req, payload, done) {
    if(!payload.isAdmin) {
        done(null, true)
    }

    done();
}



module.exports = authJwt
